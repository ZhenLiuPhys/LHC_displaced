for i in {1000_550,900_450,800_450,700_350,600_350,500_250,400_250,300_150,200_150,100_50}
do
echo $i
cp data/output.2b2tauMET_"$i".dat data_had/
for j in {0.00001,0.0001,0.001,0.002,0.005,0.01,0.02,0.05,0.1,1,2,10,20,50,100,1000}
do
echo $j
./jet_reco.exe data result3 $j 2b2tauMET_"$i"
done
done
