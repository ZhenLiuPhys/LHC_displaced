#
# Examples Makefile.
#
#                  M. Kirsanov 07.04.2006
#                     Modified 18.11.2006
#                     26.03.2008 CLHEP dependency removed

SHELL = /bin/sh

-include config.mk
#ifeq (x$(PYTHIA8LOCATION),x)
# PYTHIA8LOCATION=..
#endif

#   !!!!!!!!!  SET LOCATION OF YOUR PYTHIA8 DIRECTORY HERE  !!!!!!!!
PYTHIA8LOCATION=~/Program/pythia8186
-include $(PYTHIA8LOCATION)/config.mk


# Location of directories.
TOPDIR=$(shell \pwd)
INCDIR=include
SRCDIR=src
LIBDIR=lib
LIBDIRARCH=lib/archive
BINDIR=bin


# Libraries to include if GZIP support is enabled
ifeq (x$(ENABLEGZIP),xyes)
LIBGZIP=-L$(BOOSTLIBLOCATION) -lboost_iostreams -L$(ZLIBLOCATION) -lz
endif

# There is no default behaviour, so remind user.
all:
	@echo "Usage: for NN = example number: make mainNN"

# Create an executable for one of the normal test programs
hadronize: \
	$(PYTHIA8LOCATION)/$(LIBDIRARCH)/libpythia8.a
	@mkdir -p $(BINDIR)
	$(CXX) $(CXXFLAGS) -I$(PYTHIA8LOCATION)/$(INCDIR) $@.cc -o $(BINDIR)/$@.exe \
	-L$(PYTHIA8LOCATION)/$(LIBDIRARCH) $(LIBGZIP) -lpythia8 -llhapdfdummy
	@ln -fs $(BINDIR)/$@.exe $@.exe

checkShower: \
	$(PYTHIA8LOCATION)/$(LIBDIRARCH)/libpythia8.a
	@mkdir -p $(BINDIR)
	$(CXX) $(CXXFLAGS) -I$(PYTHIA8LOCATION)/$(INCDIR) $@.cc -o $(BINDIR)/$@.exe \
	-L$(PYTHIA8LOCATION)/$(LIBDIRARCH) $(LIBGZIP) -lpythia8 -llhapdfdummy `fastjet-config --cxxflags` `fastjet-config --libs`
	@ln -fs $(BINDIR)/$@.exe $@.exe


# Clean up: remove executables and outdated files.
.PHONY: clean
clean:
	rm -rf $(BINDIR)
	rm -rf *.exe
	rm -f *~; rm -f \#*; rm -f core*
